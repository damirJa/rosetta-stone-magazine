require('dotenv').config();
const _ = require('koa-route');
const Koa = require('koa');
const Vue = require('vue');

const template = require('fs').readFileSync('./src/index.template.html', 'utf-8');
const renderer = require('vue-server-renderer').createRenderer({
  template
});

const app = new Koa();
const {PORT} = process.env;

const vueGen = ctx => {
  const vueApp = new Vue({
    data: {
      url: ctx.req.url
    },
    template: `<div>Вы открыли URL: {{ url }}</div>`
  });
  const data = {
    title: "Mian Page"
  };
  renderer.renderToString(vueApp, data, (err, html) => {
    if (err) {
      ctx.status(500).end('Внутренняя ошибка сервера');
      return
    }
    ctx.body = html;
  })
};

app.use(_.get('*', vueGen));

app.listen(PORT);
console.log(`listening on port ${PORT}`);
